package com.splashscreen.myapplication;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class ResultReceiverActivity extends AppCompatActivity {

    public static final int RESULT_RECEIVER_ACTIVITY_REQUEST_CODE = 1234;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_receiver);

        Button button = findViewById(R.id.activity_caller_btn);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callResultReceiverActivityForResult();
            }
        });

    }

    private void callResultReceiverActivityForResult(){
        Intent intent = new Intent(this, ResultActivity.class);
        startActivityForResult(intent, RESULT_RECEIVER_ACTIVITY_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d("DEBUG", "RECEIVED " + requestCode + " / " + resultCode);

        if(requestCode == RESULT_RECEIVER_ACTIVITY_REQUEST_CODE ){

            if(resultCode == RESULT_OK){
                Log.d("DEBUG", data.getStringExtra("result"));

            }

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("DEBUG", "Activity is resuming....");
    }

}