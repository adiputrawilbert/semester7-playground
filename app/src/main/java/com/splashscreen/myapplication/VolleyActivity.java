package com.splashscreen.myapplication;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class VolleyActivity extends AppCompatActivity {

    public static int RESULT_ACTIVITY_REQUEST_CODE = 123;

    RecyclerView recyclerView;
    ArrayList<FoodReview> foodReviews;
    ArrayList<FoodReview> filteredFoodReviewList;
    FoodReviewAdapter foodReviewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_volley);

        if(getSupportActionBar() != null){
            getSupportActionBar().setTitle("Volley Activity");
        }

//        getDataFromAPI();

        TextView textView = findViewById(R.id.search_bar);
        textView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.d("DEBUG", "BEFORE TEXT " + charSequence);

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                filterResult(charSequence+"");
            }

            @Override
            public void afterTextChanged(Editable editable) {
                Log.d("DEBUG", "AFTER TEXT " + editable.toString());
            }
        });
//
//        Button nextButton = findViewById(R.id.next_button);
//        nextButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent =  new Intent(VolleyActivity.this, ResultActivity.class);
//                startActivityForResult(intent, RESULT_ACTIVITY_REQUEST_CODE);
//            }
//        });

        populateArrayList();

    }

    private void filterResult(String searchQuery){

        filteredFoodReviewList.clear();

//        for(int i = 0; i < foodReviews.size(); i++){
//            FoodReview fr = foodReviews.get(i);
//        }

        for(FoodReview fr : foodReviews){
            if(fr.getProduct().contains(searchQuery)){
                filteredFoodReviewList.add(fr);
            }
        }

        foodReviewAdapter.notifyDataSetChanged();

    }

    private void populateArrayList(){
        if (foodReviews == null) {
            foodReviews = new ArrayList<>();
        } else {
            foodReviews.clear();
        }

        if (filteredFoodReviewList == null) {
            filteredFoodReviewList = new ArrayList<>();
        } else {
            filteredFoodReviewList.clear();
        }

        foodReviews.add(new FoodReview("01", "Samsung Galaxy A", "Samsung", "Elec", "SAMSUNG GALAXY", "adflkasdflkasdf", "01001011"));
        foodReviews.add(new FoodReview("02", "Samsung Note", "Samsung", "Elec", "SAMSUNG GALAXY", "adflkasdflkasdf", "01001011"));
        foodReviews.add(new FoodReview("03", "iPHONE", "Samsung", "Elec", "SAMSUNG GALAXY", "adflkasdflkasdf", "01001011"));
        foodReviews.add(new FoodReview("04", "iMac", "Samsung", "Elec", "SAMSUNG GALAXY", "adflkasdflkasdf", "01001011"));
        foodReviews.add(new FoodReview("05", "Nokia 5", "Samsung", "Elec", "SAMSUNG GALAXY", "adflkasdflkasdf", "01001011"));
        foodReviews.add(new FoodReview("06", "Nokia 6", "Samsung", "Elec", "SAMSUNG GALAXY", "adflkasdflkasdf", "01001011"));
        foodReviews.add(new FoodReview("07", "Lenovo A2", "Samsung", "Elec", "SAMSUNG GALAXY", "adflkasdflkasdf", "01001011"));
        foodReviews.add(new FoodReview("08", "Vivo abc", "Samsung", "Elec", "SAMSUNG GALAXY", "adflkasdflkasdf", "01001011"));
        foodReviews.add(new FoodReview("09", "Product A", "Samsung", "Elec", "SAMSUNG GALAXY", "adflkasdflkasdf", "01001011"));
        foodReviews.add(new FoodReview("10", "Product A", "Samsung", "Elec", "SAMSUNG GALAXY", "adflkasdflkasdf", "01001011"));
        foodReviews.add(new FoodReview("11", "Product A", "Samsung", "Elec", "SAMSUNG GALAXY", "adflkasdflkasdf", "01001011"));
        foodReviews.add(new FoodReview("12", "Product A", "Samsung", "Elec", "SAMSUNG GALAXY", "adflkasdflkasdf", "01001011"));
        foodReviews.add(new FoodReview("13", "Product A", "Samsung", "Elec", "SAMSUNG GALAXY", "adflkasdflkasdf", "01001011"));
        foodReviews.add(new FoodReview("14", "Product A", "Samsung", "Elec", "SAMSUNG GALAXY", "adflkasdflkasdf", "01001011"));

        filteredFoodReviewList.addAll(foodReviews);

        configureRecyclerView();


    }

    private void configureRecyclerView() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
//        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);

        RecyclerView recyclerView = findViewById(R.id.recycler_view);

        foodReviewAdapter = new FoodReviewAdapter(filteredFoodReviewList, this, this);

        recyclerView.setAdapter(foodReviewAdapter);
        recyclerView.setLayoutManager(linearLayoutManager);

    }

    private void getDataFromAPI() {

        RequestQueue queue = Volley.newRequestQueue(this);

        StringRequest request = new StringRequest(
                Request.Method.GET,
                "https://thereportoftheweek-api.herokuapp.com/reports",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        Log.d("VOLLEY_ACTIVITY", response);

                        if (foodReviews == null) {
                            foodReviews = new ArrayList<>();
                        } else {
                            foodReviews.clear();
                        }

                        try {
                            JSONArray jsonArray = new JSONArray(response);

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jo = jsonArray.getJSONObject(i);
//                                Log.d("VOLLEY_ACTIVITY", jo.toString());

                                FoodReview fr = new FoodReview(
                                        jo.getString("_id"),
                                        jo.getString("product"),
                                        jo.getString("manufacturer"),
                                        jo.getString("category"),
                                        jo.getString("videoTitle"),
                                        jo.getString("videoCode"),
                                        jo.getString("dateReleased")
                                );

                                foodReviews.add(fr);

                            }

                            Log.d("VOLLEY_ACTIVITY", "Array Size : " + foodReviews.size());

                            configureRecyclerView();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Toast.makeText(VolleyActivity.this, "Network Error", Toast.LENGTH_SHORT).show();
                    }
                }
        );

        queue.add(request);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d("DEBUG", "RECEIVED " + requestCode + " / " + resultCode );

    }
}

class FoodReviewAdapter extends RecyclerView.Adapter<FoodReviewViewHolder> {

    ArrayList<FoodReview> foodReviews;
    Context context;
    AppCompatActivity activity;

    public FoodReviewAdapter(ArrayList<FoodReview> names, Context context, AppCompatActivity activity) {
        this.foodReviews = names;
        this.context = context;
        this.activity = activity;
    }

    @NonNull
    @Override
    public FoodReviewViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        Context mContext = parent.getContext();
        LayoutInflater mLayoutInflater = LayoutInflater.from(mContext);
        View view = mLayoutInflater.inflate(R.layout.food_review_view_holder, parent, false);
        return new FoodReviewViewHolder(view);

    }

    @Override
    public int getItemCount() {
        if (foodReviews != null) {
            return foodReviews.size();
        }
        return 0;
    }

    @Override
    public void onBindViewHolder(@NonNull final FoodReviewViewHolder holder, int position) {

        final FoodReview fr = foodReviews.get(position);

        holder.titleView.setText(fr.getProduct());
        holder.videoTitleView.setText(fr.getVideoTitle());
        holder.dateView.setText(fr.getDateReleased());

        Glide.with(context).load("https://img.youtube.com/vi/" + fr.getVideoCode() + "/0.jpg").into(holder.imageView);

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ItemDetailDialogFragment itemDetailDialogFragment = ItemDetailDialogFragment.newInstance(fr);
                itemDetailDialogFragment.show(activity.getSupportFragmentManager(), null);


                /*
                ViewGroup viewGroup = holder.rootView.findViewById(android.R.id.content);
                View dialogView = LayoutInflater.from(context).inflate(R.layout.item_detail_dialog, viewGroup ,false);

                ImageView imageView = dialogView.findViewById(R.id.video_image_view);
                TextView titleView = dialogView.findViewById(R.id.title_text_view);
                TextView subTitleView = dialogView.findViewById(R.id.sub_title_text_view);

                titleView.setText(fr.getProduct());
                subTitleView.setText(fr.getVideoTitle());

                Glide.with(dialogView).load("https://img.youtube.com/vi/" + fr.getVideoCode() + "/0.jpg").into(imageView);


                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setView(dialogView);

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
                */
            }
        });

        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(context, "ITEM WITH ID " + fr.getId() + " is pressed!", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(context, VideoDetailActivity.class);
                Bundle params = new Bundle();
                params.putString("productName", fr.getProduct());
                params.putString("videoTitle", fr.getVideoTitle());
                params.putString("videoCode", fr.getVideoCode());
                intent.putExtras(params);
                context.startActivity(intent);
            }
        });
    }
}

class FoodReviewViewHolder extends RecyclerView.ViewHolder {

    ImageView imageView;
    TextView titleView;
    TextView videoTitleView;
    TextView dateView;

    View rootView;

    public FoodReviewViewHolder(@NonNull View itemView) {
        super(itemView);

        rootView = itemView;

        imageView = itemView.findViewById(R.id.image_view);
        titleView = itemView.findViewById(R.id.title_text_view);
        videoTitleView = itemView.findViewById(R.id.video_title_text_view);
        dateView = itemView.findViewById(R.id.date_text_view);

    }

}