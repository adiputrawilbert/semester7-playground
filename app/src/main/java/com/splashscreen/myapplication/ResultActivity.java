package com.splashscreen.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ResultActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result2);

        EditText editText = findViewById(R.id.result_text_field);
        Button submitButton = findViewById(R.id.back_button);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String result = editText.getText().toString();

                Intent intent = new Intent();
                intent.putExtra("result", result);

                setResult(RESULT_OK, intent);
                finish();
            }
        });

    }


}