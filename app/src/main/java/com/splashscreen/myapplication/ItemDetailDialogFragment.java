package com.splashscreen.myapplication;

import android.app.Dialog;
import androidx.fragment.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;

public class ItemDetailDialogFragment extends DialogFragment {

    FoodReview fr;

    public FoodReview getFoodReview() {
        return fr;
    }

    public void setFoodReview(FoodReview foodReview) {
        this.fr = foodReview;
    }

    public static ItemDetailDialogFragment newInstance(FoodReview fr) {
        ItemDetailDialogFragment productDetailDialog = new ItemDetailDialogFragment();

        productDetailDialog.setFoodReview(fr);

        return productDetailDialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        return super.onCreateDialog(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.item_detail_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ImageView imageView = view.findViewById(R.id.video_image_view);
        TextView titleView = view.findViewById(R.id.title_text_view);
        TextView subTitleView = view.findViewById(R.id.sub_title_text_view);

        setCancelable(true);

        titleView.setText(fr.getProduct());
        subTitleView.setText(fr.getVideoTitle());

        Glide.with(view).load("https://img.youtube.com/vi/" + fr.getVideoCode() + "/0.jpg").into(imageView);
    }


}
