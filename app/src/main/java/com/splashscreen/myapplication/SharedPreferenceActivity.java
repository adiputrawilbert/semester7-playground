package com.splashscreen.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class SharedPreferenceActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_preference);

        SharedPreferences settingPreference = getSharedPreferences("settings", Context.MODE_PRIVATE);
        String title = settingPreference.getString("title_bar_string", "Default String");

        getSupportActionBar().setTitle(title);

        Button btn = findViewById(R.id.submit_btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText titleEdit = findViewById(R.id.title_bar_edit_text);
                updateTitle(titleEdit.getText().toString());


            }
        });

    }

    private void updateTitle(String newTitle){

        SharedPreferences settingPreference = getSharedPreferences("settings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settingPreference.edit();
        editor.putString("title_bar_string", newTitle);
        editor.apply();

    }



}