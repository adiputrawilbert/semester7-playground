package com.splashscreen.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        ArrayList<Student> names = new ArrayList<Student>();

        names.add(new Student("Wilbert", "Angkatan 2015"));
        names.add(new Student("Tyo", "Angkatan 2017"));
        names.add(new Student("Danar", "Angkatan 2017"));
        names.add(new Student("Andre", "Angkatan 2017"));
        names.add(new Student("Wilbert", "Angkatan 2015"));
        names.add(new Student("Tyo", "Angkatan 2017"));
        names.add(new Student("Danar", "Angkatan 2017"));
        names.add(new Student("Andre", "Angkatan 2017"));
        names.add(new Student("Wilbert", "Angkatan 2015"));
        names.add(new Student("Tyo", "Angkatan 2017"));
        names.add(new Student("Danar", "Angkatan 2017"));
        names.add(new Student("Andre", "Angkatan 2017"));

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);

        RecyclerView recyclerView = findViewById(R.id.list_activity_recycler_view);
        recyclerView.setAdapter(new CardInfoAdapter(names));
        recyclerView.setLayoutManager(linearLayoutManager);

    }

}

class CardInfoAdapter extends RecyclerView.Adapter<CardInfoViewHolder>{

    ArrayList<Student> names;

    public CardInfoAdapter(ArrayList<Student> names){
        this.names = names;
    }

    @NonNull
    @Override
    public CardInfoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        Context mContext = parent.getContext();
        LayoutInflater mLayoutInflater = LayoutInflater.from(mContext);

        View view = mLayoutInflater.inflate(R.layout.card_info_view_holder, parent, false);

        return new CardInfoViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull CardInfoViewHolder holder, int position) {
        holder.textView.setText(names.get(position).getName());
        holder.descriptionTextView.setText(names.get(position).getDescription());
    }

    @Override
    public int getItemCount() {
        if(names != null){
            return names.size();
        }
        return 0;
    }

}

class CardInfoViewHolder extends RecyclerView.ViewHolder {

    TextView textView;
    TextView descriptionTextView;

    public CardInfoViewHolder(@NonNull View itemView) {
        super(itemView);

        textView = itemView.findViewById(R.id.text_view);
        descriptionTextView = itemView.findViewById(R.id.description_text_view);

    }

}

class Student {

    private String name;
    private String description;

    public Student(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}