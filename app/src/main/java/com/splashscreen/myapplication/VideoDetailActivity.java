package com.splashscreen.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VideoDetailActivity extends AppCompatActivity {

    @BindView(R.id.video_image_view) ImageView imageView;
    @BindView(R.id.title_text_view) TextView titleView;
    @BindView(R.id.sub_title_text_view) TextView subTitleView;

    @OnClick(R.id.title_text_view)
    void onTitleClicked(){
        Toast.makeText(VideoDetailActivity.this, "Hello World", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_detail);

        ButterKnife.bind(this);

        if(getSupportActionBar() != null){
            getSupportActionBar().setTitle("Detail");
        }

        Bundle params = getIntent().getExtras();

        titleView.setText(params.getString("productName"));
        subTitleView.setText(params.getString("videoTitle"));

        Glide.with(this).load("https://img.youtube.com/vi/" + params.getString("videoCode") + "/0.jpg").into(imageView);

    }

}