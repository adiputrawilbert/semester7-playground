package com.splashscreen.myapplication;

public class FoodReview {

    private String id;
    private String product;
    private String manufacturer;
    private String category;

    private String videoTitle;
    private String videoCode;
    private String dateReleased;

    public FoodReview(String id, String product, String manufacturer, String category, String videoTitle, String videoCode, String dateReleased) {
        this.id = id;
        this.product = product;
        this.manufacturer = manufacturer;
        this.category = category;
        this.videoCode = videoCode;
        this.videoTitle = videoTitle;
        this.dateReleased = dateReleased;
    }

    public String getId() {
        return id;
    }

    public String getProduct() {
        return product;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getCategory() {
        return category;
    }

    public String getVideoCode() {
        return videoCode;
    }

    public String getDateReleased() {
        return dateReleased;
    }

    public String getVideoTitle() {
        return videoTitle;
    }
}
